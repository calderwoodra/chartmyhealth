/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.manyhands.checkup.R;
import com.manyhands.checkup.SharedPrefs;
import com.manyhands.checkup.activity.Fragments.HealthPlanList;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.HealthPlan;
import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Reminder;
import com.manyhands.checkup.network.Network;

/**
 * Created by Allen on 10/25/2014.
 */
public class ActivityMain extends Activity implements View.OnClickListener{
    private ActionBar actionBar;
    private ActionBarDrawerToggle drawerListener;
    private DrawerLayout drawerLayout;

    private FragmentManager fm;
    private FragmentTransaction ft;
    private SharedPrefs sharedPrefs;
    HealthPlanList healthPlanListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPrefs = new SharedPrefs(this);
        if(sharedPrefs.getSession() == null) {
            String inviteCode = "";
            Bundle bundle = getIntent().getExtras();

            if(bundle != null)
                inviteCode = bundle.getString(Network.INVITE_CODE, "");

            Intent intent = new Intent(this, ActivityLoginSignup.class);
            intent.putExtra(Network.INVITE_CODE, inviteCode);
            startActivity(intent);
        }
        else {
            //loadScheduleTest(null);
            Network.setSession(sharedPrefs.getSession());
            Network.setContext(this);
            setContentView(R.layout.main_activity);
            init();
            setListeners();
            setupDrawer();
        }
    }

    @Override
    public void onClick(View v) {

    }

    private void init(){
        actionBar = getActionBar();
        fm = getFragmentManager();
        ft = fm.beginTransaction();
        healthPlanListFragment = new HealthPlanList();
        ft.replace(R.id.mainContent, healthPlanListFragment);
        ft.commit();
    }

    private void setListeners(){

    }

    private void setupDrawer(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        drawerListener = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.btn_nav_drawer, 0, 0){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerListener);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }

    public void logout(View v) {
        boolean loggedOut = Network.logout(sharedPrefs.getSession());
        if(loggedOut) {
            sharedPrefs.removeSession();
            Intent intent = new Intent(this, ActivityLoginSignup.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public void loadTestData(MenuItem item) {
        HealthPlan healthPlan = HealthPlan.create(this, "2321342", "Test HP2", "This is a test also.");
        Form form = Form.create(this, healthPlan, "412321423", "Form 3", "Submit");
        Question qSort = Question.createSort(this, form, "96858", "Sort from lowest to highest.", new String[]{"5", "3", "1", "4", "2"});
        Question qSelect = Question.createSelect(this, form, "23454325243543", "Which is number 1?", new String[]{"5", "3", "1", "2", "5"});
        Question qRate = Question.createRate(this, form, "87996798767896", "How much does it hurt, the pain?", 0, 10);
        Question qCheckbox = Question.createCheckbox(this, form, "547564745457447", "Select the colors you like.", new String[]{"red", "green", "blue", "black", "pink", "white", "orange", "yellow", "cyan", "magenta", "brown"});
        Reminder reminder = Reminder.create(this, healthPlan, "23j232l3h", "Reminder 1");
        Reminder reminder2 = Reminder.create(this, healthPlan, "23j234233h", "Reminder 2");

        healthPlanListFragment.updateHP();
    }

    public void loadScheduleTest(MenuItem item) {
        Intent intent = new Intent(this, ScheduleTest.class);
        startActivity(intent);
    }
}

/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.manyhands.checkup.R;

/**
 * Created by Allen on 9/15/2014.
 */
public class VoiceCommand extends Activity {

    TextView tvVoiceMessage;
    public static String voiceMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_command_display);

        tvVoiceMessage = (TextView) findViewById(R.id.tvVoiceMessage);

        Intent intent = getIntent();
		
        voiceMessage = getMessageText(intent);
    }

    //Current issue with min API being 19(highest irl API) and method requiring API 20
    private String getMessageText(Intent intent){
        ClipData extra = intent.getClipData();
        Log.d("TAG", "" + extra.getItemCount());
        ClipData.Item item = extra.getItemAt(0);

        Log.d("TEXT", "" + item.getText());
        Log.d("URI", "" + item.getUri());
        Log.d("INTENT", "" + item.getIntent());

        Bundle extras = item.getIntent().getExtras();
        Bundle bundle = extras.getBundle("android.remoteinput.resultsData");

        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            Log.d("TAG", String.format("%s %s (%s)", key,
                    value.toString(), value.getClass().getName()));
        }

        tvVoiceMessage.setText(bundle.get("set text").toString());

        return bundle.get("set text").toString();
    }
}

package com.manyhands.checkup.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.manyhands.checkup.R;
import com.manyhands.checkup.SharedPrefs;
import com.manyhands.checkup.network.Network;
import com.manyhands.checkup.network.Session;

/**
 * Created by Allen on 10/25/2014.
 */
public class FragmentSignup extends Fragment {

    EditText etUsername, etPassword, etAccessCode;
    CheckBox cbShowPassword;
    Button bSignup;

    boolean showPassword = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        init(view);
        setListeners();
        return view;
    }

    private void init(View view){
        etUsername = (EditText) view.findViewById(R.id.etUsername);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        etAccessCode = (EditText) view.findViewById(R.id.etInviteCode);
        cbShowPassword = (CheckBox) view.findViewById(R.id.cbShowPassword);
        bSignup = (Button) view.findViewById(R.id.bSignup);

        Bundle bundle = getActivity().getIntent().getExtras();
        if(bundle != null)
            etAccessCode.setText(getActivity().getIntent().getExtras().getString(Network.INVITE_CODE, ""));
    }

    private void setListeners(){
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(showPassword){
                    showPassword = false;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etPassword.setSelection(etPassword.getText().toString().length());
                }else{
                    showPassword = true;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etPassword.setSelection(etPassword.getText().toString().length());
                }
            }
        });

        bSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefs sharedPrefs = new SharedPrefs(getActivity());
                String email = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                String accessCode = etAccessCode.getText().toString();
                Session session = Network.register(email, password, accessCode);
                if(session != null){
                    sharedPrefs.setSession(session);
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getActivity(), "Error creating account", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

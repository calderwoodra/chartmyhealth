/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.manyhands.checkup.R;
import com.manyhands.checkup.Scheduler;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.HealthPlan;
import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Reminder;
import com.manyhands.checkup.model.Schedule;
import com.manyhands.checkup.model.ScheduledEvent;

import java.util.ArrayList;

public class ScheduleTest extends Activity {
    public static final int MAX_SCHEDULED_EVENTS = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_test);
        HealthPlan healthPlan = HealthPlan.create(this, "2321342", "Test HP2", "This is a test also.");
        Form form = Form.create(this, healthPlan, "412321423", "Form 3", "Submit");
        Question qSort = Question.createSort(this, form, "96858", "Sort from lowest to highest.", new String[]{"5", "3", "1", "4", "2"});
        Question qSelect = Question.createSelect(this, form, "23454325243543", "Which is number 1?", new String[]{"5", "3", "1", "2", "5"});
        Question qRate = Question.createRate(this, form, "87996798767896", "How much does it hurt, the pain?", 0, 10);
        Question qCheckbox = Question.createCheckbox(this, form, "547564745457447", "Select the colors you like.", new String[]{"red", "green", "blue", "black", "pink", "white", "orange", "yellow", "cyan", "magenta", "brown"});
        Reminder reminder = Reminder.create(this, healthPlan, "23j232l3h", "You need to be reminded");
        Schedule schedule = Schedule.create(this, healthPlan, "s6");

        long begin = System.currentTimeMillis();
        long end = begin + Scheduler.MILLISECONDS_IN_SECONDS * Scheduler.SECONDS_IN_MINUTES;
        long interval = Scheduler.MILLISECONDS_IN_SECONDS * 10; // 10 second intervals

        //ScheduledEvent s1 = new ScheduledEvent("s1", Schedule.Type.REMINDER, begin + Scheduler.MILLISECONDS_IN_SECONDS * 15);
        //ScheduledEvent s2 = new ScheduledEvent("s2", Schedule.Type.REMINDER, begin, interval, end);
        //ScheduledEvent s3 = new ScheduledEvent("s3", Schedule.Type.REMINDER, begin + Scheduler.MILLISECONDS_IN_SECONDS * 25);
        //ScheduledEvent s4 = new ScheduledEvent("s4", Schedule.Type.REMINDER, begin, interval*2, end);
        //ScheduledEvent s5 = new ScheduledEvent("s5", Schedule.Type.REMINDER, begin, 2000);

        ScheduledEvent se1 = ScheduledEvent.create(this, schedule, form.getUuid(), ScheduledEvent.Type.FORM, begin + Scheduler.MILLISECONDS_IN_SECONDS * 15, 2000);

        /*ArrayList<ScheduledEvent> list = new ArrayList<ScheduledEvent>();
        ArrayList<ScheduledEvent> list2 = new ArrayList<ScheduledEvent>();
        list.add(s1);
        list.add(s2);
        list2.add(s3);
        list2.add(s4);
        list2.add(s5);
        Schedule schedule = new Schedule(list);
        Schedule schedule2 = new Schedule(list2);*/

        Scheduler.loadSchedules(this);
        Scheduler.populate(MAX_SCHEDULED_EVENTS);


        while(true) {
            Log.d("ScheduleTest", "repopulate");
            Scheduler.populate(7);

            while (!Scheduler.isEmpty()) {
                Log.d("ScheduleTest", Scheduler.deQueue().toString());
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException ie){
                    ie.printStackTrace();
                }
            }


        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.schedule_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

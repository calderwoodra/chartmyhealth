/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity.ListAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.manyhands.checkup.R;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.question.QuestionCheckbox;
import com.manyhands.checkup.model.question.QuestionRate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Allen on 10/26/2014.
 */
public class FormListAdapter extends BaseAdapter {

    Context context;
    Form form;
    ArrayList<Question> questions;
    TextView tvQuesion;
    Question question;

    HashMap<View, Question> questionMap;

    public FormListAdapter(Context context, Form form){
        this.context = context;
        this.form = form;
        //questions = form.getQuestionsAL();
        questions = new ArrayList<Question>();
        questions.add(new QuestionCheckbox(form, "112212", "Any side effects?", new String[]{"nauseous, dizziness, headache"}));
        questions.add(new QuestionRate(form, "123123", "Treatment satisfaction?", 0, 5));
        questionMap = new HashMap<View, Question>();
    }

    @Override
    public int getCount() {
        return questions!=null?questions.size():0;
    }

    @Override
    public Object getItem(int position) {
        return questions.get(position);
//        Iterator<Question> it = questions.iterator();
//
//        Question q;
//        for(int i = -1; i < position; i++){
//            q = (Question)it.next();
//            Log.d("questoin", q.getUuid());
//        }
//
//        Log.d("exists", it.hasNext() + "");
//        q = (Question)it.next();
//        Log.d("to return", q.getUuid());
//        return q;


        //return questions!=null?questions.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        question = questions.get(position);


        View view = null;

        if(question.getType().equals(Question.Type.CHECKBOX)){
            view = getCheckboxQuestion(inflater, parent);
        }else if(question.getType().equals(Question.Type.RATE)){
            view = getRateQuestion(inflater, parent);
        }else if(question.getType().equals(Question.Type.SELECT)){
            //view = getSelectQuestion(view, inflater, parent);
        }else if(question.getType().equals(Question.Type.SORT)){
           // view = getSortQuestion(view, inflater, parent);
        }

        return view;
    }

    private View getCheckboxQuestion(LayoutInflater inflater, ViewGroup parent){
        View view = inflater.inflate(R.layout.row_checkbox_question, parent, false);
        tvQuesion = (TextView) view.findViewById(R.id.tvQuestion);
        tvQuesion.setText(question.getText());

        final QuestionCheckbox currentQuestion = (QuestionCheckbox) question;
        questionMap.put(view, currentQuestion);
        String[] options = currentQuestion.getOptions();



//        LayoutInflater inflate2 =
//
//        for(String option : options){
//
//            final CheckBox cbOption = (CheckBox) //inflater.inflate(R.layout.checkbox_option, ((LinearLayout)view.findViewById(R.id.llCheckboxOptions)), false);
//            cbOption.setText(option);
//            cbOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    if (isChecked){
//                        currentQuestion.addOptionSelected(cbOption.getText().toString());
//                    }else{
//                        currentQuestion.removeOptionSelected(cbOption.getText().toString());
//                    }
//                }
//            });
//        }
        return view;
    }

    private View getRateQuestion(LayoutInflater inflater, ViewGroup parent){
        View view = inflater.inflate(R.layout.row_rate_question, parent, false);
        tvQuesion = (TextView) view.findViewById(R.id.tvQuestion);
        tvQuesion.setText(question.getText());

        final QuestionRate currentQuestion = (QuestionRate) question;
        questionMap.put(view, currentQuestion);

        SeekBar rate = (SeekBar) view.findViewById(R.id.seekRate);
        rate.setMax(currentQuestion.getMax());

        rate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    currentQuestion.setSelectedRate(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        return view;
    }

    private View getSortQuestion(View view, LayoutInflater inflater, ViewGroup parent){
        return null;
    }

    private View getSelectQuestion(View view, LayoutInflater inflater, ViewGroup parent){
        return null;
    }
}

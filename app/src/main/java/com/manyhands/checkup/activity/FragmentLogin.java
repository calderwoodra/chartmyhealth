/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.manyhands.checkup.R;
import com.manyhands.checkup.SharedPrefs;
import com.manyhands.checkup.network.Network;
import com.manyhands.checkup.network.Session;

/**
 * Created by Allen on 10/25/2014.
 */
public class FragmentLogin extends Fragment{


    EditText etUsername, etPassword;
    CheckBox cbShowPassword;
    Button bLogin;

    boolean showPassword = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        init(view);
        setListeners();
        return view;
    }

    private void init(View view){
        etUsername = (EditText) view.findViewById(R.id.etUsername);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        cbShowPassword = (CheckBox) view.findViewById(R.id.cbShowPassword);
        bLogin = (Button) view.findViewById(R.id.bLogin);
    }

    private void setListeners(){
        cbShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(showPassword){
                    showPassword = false;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    etPassword.setSelection(etPassword.getText().toString().length());
                }else{
                    showPassword = true;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    etPassword.setSelection(etPassword.getText().toString().length());
                }
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefs sharedPrefs = new SharedPrefs(getActivity());
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                Session session = Network.login(username, password);
                if(session != null) {
                    sharedPrefs.setSession(session);
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
                }
                else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Incorrect login information").setPositiveButton("Ok", null).show();
                }
            }
        });
    }
}

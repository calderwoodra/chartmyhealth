/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity.ListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.manyhands.checkup.R;
import com.manyhands.checkup.model.HealthPlan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Allen on 10/25/2014.
 */
public class HealthPlanAdapter extends BaseAdapter {

    HashMap<View, HealthPlan> hpMap;
    List<HealthPlan> healthPlans;
    TextView tvName, tvDescription;
    Context context;

    public HealthPlanAdapter(Context context){
        this.context = context;
        healthPlans = HealthPlan.loadAll(context);
        hpMap = new HashMap<View, HealthPlan>();
    }

    @Override
    public int getCount() {
        return healthPlans!=null?healthPlans.size():0;
    }

    @Override
    public Object getItem(int position) {
        return healthPlans!=null?healthPlans.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_health_plan, parent, false);
        tvName = (TextView) row.findViewById(R.id.tvHealthPlanName);
        tvDescription = (TextView) row.findViewById(R.id.tvHealthPlanDescription);
        HealthPlan currentHP = healthPlans.get(position);

        tvName.setText(currentHP.getName());
        tvDescription.setText(currentHP.getDescription());

        hpMap.put(row, currentHP);

        return row;
    }

    public HealthPlan getHealthPlan(View view){
        return hpMap.get(view);
    }
}

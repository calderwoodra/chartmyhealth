/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity.Fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.manyhands.checkup.R;
import com.manyhands.checkup.activity.ListAdapters.FormListAdapter;
import com.manyhands.checkup.form.FormAdapter;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.HealthPlan;

/**
 * Created by Allen on 10/26/2014.
 */
public class FormFragment extends ListFragment {

    FormListAdapter adapter;
    Form form;
    Button bSubmit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, container, false);
        init(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new FormListAdapter(getActivity(), form);
        getListView().setAdapter(adapter);
    }

    private void init(View view){
        bSubmit = (Button) view.findViewById(R.id.bSubmit);
        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack("HealthPlanList", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });
    }

    public void setForm(Form form){
        this.form = form;
    }

}

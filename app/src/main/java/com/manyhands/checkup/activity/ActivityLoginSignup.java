/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.manyhands.checkup.R;
import com.manyhands.checkup.network.Network;

/**
 * Created by Allen on 10/25/2014.
 */
public class ActivityLoginSignup extends Activity implements ActionBar.TabListener{

    protected static final int LOGIN_TAB = 0;
    protected static final int SIGNUP_TAB = 1;

    protected ActionBar.Tab loginTab, signupTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Network.setContext(this);
        setupActionBar();
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        switch(tab.getPosition()) {
            case LOGIN_TAB:
                FragmentLogin fragmentLogin = new FragmentLogin();
                ft.replace(android.R.id.tabcontent, fragmentLogin);
                break;

            case SIGNUP_TAB:
                FragmentSignup fragmentSignup = new FragmentSignup();
                ft.replace(android.R.id.tabcontent, fragmentSignup);
                break;
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        switch(tab.getPosition()) {
            case LOGIN_TAB:
                break;
            case SIGNUP_TAB:
                break;
        }
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {}

    private void setupActionBar(){
        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        loginTab = actionBar.newTab().setText("Login");
        signupTab = actionBar.newTab().setText("Sign Up");

        loginTab.setTabListener(this);
        signupTab.setTabListener(this);

        actionBar.addTab(loginTab);
        actionBar.addTab(signupTab);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}

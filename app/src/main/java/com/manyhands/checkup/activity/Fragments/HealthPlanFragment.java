/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity.Fragments;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.manyhands.checkup.R;
import com.manyhands.checkup.activity.ListAdapters.EventListAdapter;
import com.manyhands.checkup.model.Biometric;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.HealthPlan;
import com.manyhands.checkup.model.Reminder;
import com.manyhands.checkup.old.FormFragment;

import java.util.ArrayList;

/**
 * Created by Allen on 10/26/2014.
 */
public class HealthPlanFragment extends ListFragment implements AdapterView.OnItemClickListener{

    HealthPlan healthPlan;
    EventListAdapter adapter;
    Spinner spinnerForms;
    LinearLayout llBiometrics;
    Form[] forms;


    public void setHealthPlan(HealthPlan healthPlan){

        this.healthPlan = healthPlan;
        forms = healthPlan.getForms();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_plan, container, false);
        init(view);
        return view;
    }

    private void init(View view){
        spinnerForms = (Spinner) view.findViewById(R.id.spinnerForms);

        ArrayList<String> formNames = new ArrayList<String>();
        formNames.add("Forms");
        for(Form item : forms){
            formNames.add(item.getName());
        }

        spinnerForms.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item,
                formNames));

        spinnerForms.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!((TextView) view).getText().toString().equals("Forms")){
                    com.manyhands.checkup.activity.Fragments.FormFragment formFragment = new com.manyhands.checkup.activity.Fragments.FormFragment();
                    formFragment.setForm(forms[position-1]);
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.addToBackStack("health plan");
                    ft.replace(R.id.mainContent, formFragment);
                    ft.commit();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //needs to be changed to get the biometrics from a health plan
        ArrayList<String> biometrics = new ArrayList<String>();
        biometrics.add("99");
        biometrics.add("91");
        biometrics.add("89");
        biometrics.add("107");
        biometrics.add("60");
        biometrics.add("75");
        biometrics.add("95");
        biometrics.add("71");
        biometrics.add("88");
        biometrics.add("92");
        biometrics.add("87");
        biometrics.add("81");

        llBiometrics = (LinearLayout) view.findViewById(R.id.llBiometrics);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        for(String biometric : biometrics){
            View biometricRow = inflater.inflate(R.layout.row_biometric, null);
            ((TextView)biometricRow.findViewById(R.id.tvBiometricValue)).setText(biometric);    //needs to get a value
            llBiometrics.addView(biometricRow);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Reminder> reminders = new ArrayList<Reminder>();
        reminders.addAll(healthPlan.getReminders());

        adapter = new EventListAdapter(getActivity(), reminders);
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Load event item details...
    }
}

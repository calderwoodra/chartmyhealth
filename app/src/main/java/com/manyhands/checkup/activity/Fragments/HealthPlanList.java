/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity.Fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;

import com.manyhands.checkup.R;
import com.manyhands.checkup.activity.ListAdapters.HealthPlanAdapter;
import com.manyhands.checkup.activity.VoiceCommand;
import com.manyhands.checkup.model.HealthPlan;

/**
 * Created by Allen on 10/25/2014.
 */
public class HealthPlanList extends ListFragment implements AdapterView.OnItemClickListener{

    HealthPlanAdapter adapter;
    Button bAddHealthPlan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_plan_list, container, false);
        bAddHealthPlan = (Button) view.findViewById(R.id.bAddHealthPlan);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new HealthPlanAdapter(getActivity());
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
        setListeners();
    }

    public void updateHP(){
        adapter.notifyDataSetChanged();
    }

    private void setListeners(){
        bAddHealthPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotification("Reminder", "Answer Assessment");
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HealthPlan selectedHP = adapter.getHealthPlan(view);
        FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
        HealthPlanFragment healthPlanFragment = new HealthPlanFragment();
        healthPlanFragment.setHealthPlan(selectedHP);
        ft.addToBackStack("HealthPlanList");
        ft.replace(R.id.mainContent, healthPlanFragment);
        ft.commit();
    }

    private void sendNotification(String title, String content){

        // Key that is bundled with the voice reply intent, Value is the voice reply
        final String EXTRA_VOICE_REPLY = "set text";

        String replyLabel = "Voice Reply";
        String[] replyChoices = {"Call Successful, no follow up needed.", "Researching solution, must call back today", "Missed call, will call back"};

        RemoteInput remoteInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY)
                .setLabel(replyLabel)       //sets the label for the voice action
                .setChoices(replyChoices)   //sets some of the default reply choices
                .build();


        Intent replyIntent = new Intent(getActivity(), VoiceCommand.class);
        replyIntent.setAction("Voice Command");
        PendingIntent replyPendingIntent = PendingIntent.getActivity(getActivity(), 0, replyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.drawable.ic_reply_icon,
                        replyLabel, replyPendingIntent)
                        .addRemoteInput(remoteInput)
                        .build();

        int notificationId = 001;
        Intent viewIntent = new Intent(); //new Intent(this, Activity to be opened by notification);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(getActivity(), 0, viewIntent, 0);

        //Making the notification "Big Style", This allows for larger than normal notifications,
        //Which I imagine will be the case for our application.
        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.bigText(content);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(getActivity())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(content)
                        .setContentIntent(viewPendingIntent)
                        .extend(new NotificationCompat.WearableExtender().addAction(action))   //This is where we add the voice reply action to our notification
                        .setStyle(bigStyle);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getActivity());

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}

/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.activity.ListAdapters;

import android.content.Context;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.manyhands.checkup.R;
import com.manyhands.checkup.model.Reminder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Allen on 10/26/2014.
 */
public class EventListAdapter extends BaseAdapter{

    ArrayList<Reminder> reminders;
    Context context;
    TextView tvEventName;

    public EventListAdapter(Context context, ArrayList<Reminder> reminders){
        this.context = context;
        this.reminders = reminders;
    }

    @Override
    public int getCount() {
        return reminders!=null?reminders.size():0;
    }

    @Override
    public Object getItem(int position) {
        return reminders!=null?reminders.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_event, parent, false);
        tvEventName = (TextView) row.findViewById(R.id.tvEventName);
        tvEventName.setText(reminders.get(position).getDescription());

        return row;
    }
}

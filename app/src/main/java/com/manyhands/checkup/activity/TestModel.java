package com.manyhands.checkup.activity;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.manyhands.checkup.AlarmReceiver;
import com.manyhands.checkup.R;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.HealthPlan;
import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Reminder;

import java.util.Calendar;
import java.util.List;

public class TestModel extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_model);

        Gson gson = new Gson();

        //String json = "";
        //importHealthPlan(json);

        //HealthPlan healthPlan = createTestHealthPlan();

        try {
            HealthPlan healthPlan = HealthPlan.create(this, "2321342", "Test HP2", "This is a test also.");
            Form form = Form.create(this, healthPlan, "412321423", "Form 3", "Submit");
            Question qSort = Question.createSort(this, form, "96858", "Sort from lowest to highest.", new String[]{"5", "3", "1", "4", "2"});
            Question qSelect = Question.createSelect(this, form, "23454325243543", "Which is number 1?", new String[]{"5", "3", "1", "2", "5"});
            Question qRate = Question.createRate(this, form, "87996798767896", "How much does it hurt, the pain?", 0, 10);
            Question qCheckbox = Question.createCheckbox(this, form, "547564745457447", "Select the colors you like.", new String[]{"red", "green", "blue", "black", "pink", "white", "orange", "yellow", "cyan", "magenta", "brown"});
        }catch(Exception e){
            //error
        }

        //Log.d("TestModel", gson.toJson(hp).toString());
        //Log.d("TestModel", "done");

        // get a Calendar object with current time
        Calendar cal = Calendar.getInstance();
        // add 5 minutes to the calendar object
        cal.add(Calendar.MINUTE, 5);
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("alarm_message", "O'Doyle Rules!");
        // In reality, you would want to have a static variable for the request code instead of 192837
        PendingIntent sender = PendingIntent.getBroadcast(this, 192837, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Get the AlarmManager service
        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
    }

    /*public HealthPlan createTestHealthPlan() {
        HealthPlan healthPlan = new HealthPlan("23142341324123421342", "Test HP", "This is a test").save(this);

        List<Question> questions = new ArrayList<Question>();
        questions.add(new QuestionSelect("23454325243543", "Which is number 1?", new String[]{"5", "3", "1", "2", "5"}).save(this));
        questions.add(new QuestionRate("87996798767896", "How much does it hurt, the pain?", 0, 10).save(this));
        questions.add(new QuestionCheckbox("547564745457447", "Select the colors you like.", new String[]{"red", "green", "blue", "black", "pink", "white", "orange", "yellow", "cyan", "magenta", "brown"}).save(this));
        //questions.add(new QuestionSort("968589712334214", "Sort from lowest to highest.", new String[]{"5","3", "1", "4", "2"}).save(this));

        Form form = new Form(healthPlan, "412342141324321423", "Form 1", "Submit");
        for(Question question : questions)
            form.addQuestion(questions.get(0));
        form.save(this);

        Form form2 = new Form(healthPlan, "4123412342134423", "Form 2", "GO!");
        form2.addQuestion(questions.get(0)).addQuestion(questions.get(4)).save(this);

        //healthPlan.addForm(form2).save(this);

        return healthPlan;
    }*/

    public void importHealthPlan(String json) {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray jsonArray = parser.parse(json).getAsJsonArray();

        String uuid = gson.fromJson(jsonArray.get(0), String.class);
        String name = gson.fromJson(jsonArray.get(1), String.class);
        String description = gson.fromJson(jsonArray.get(2), String.class);

        HealthPlan healthPlan = HealthPlan.create(this, uuid, name, description);
        List<Form> forms = gson.fromJson(jsonArray.get(3), new TypeToken<List<Form>>() {}.getType());
        //List<Question> question = gson.fromJson(jsonArray.get(3), new TypeToken<List<Question>>() {}.getType());
        List<Reminder> reminders = gson.fromJson(jsonArray.get(3), new TypeToken<List<Reminder>>() {}.getType());

        healthPlan.save(this);
    }

    public void importForms(JsonArray jsonArray) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }
}
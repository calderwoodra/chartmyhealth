package com.manyhands.checkup;

import android.content.Context;
import android.content.SharedPreferences;

import com.manyhands.checkup.network.Session;

public class SharedPrefs {
    public static final String SESSION_ID = "session_id";
    public static final String SESSION_NAME = "session_name";
    public static final String TOKEN = "auth_token";


    private static final String prefsFileName = "prefs";
    private SharedPreferences prefs;

    public SharedPrefs(Context context) {
        prefs = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences() {
        return prefs;
    }

    public void setSession(Session session) {
        prefs.edit().putString(SESSION_ID, session.getSessionId()).apply();
        prefs.edit().putString(SESSION_NAME, session.getSessionName()).apply();
        prefs.edit().putString(TOKEN, session.getToken()).apply();
    }

    public Session getSession() {
        String sessionId = prefs.getString(SharedPrefs.SESSION_ID, "");
        String sessionName = prefs.getString(SharedPrefs.SESSION_NAME, "");
        String authToken = prefs.getString(SharedPrefs.TOKEN, "");

        if(sessionId.length() > 0 && sessionName.length() > 0 && authToken.length() > 0)
            return new Session(sessionId, sessionName, authToken);

        return null;
    }

    public void removeSession() {
        prefs.edit().remove(SESSION_ID).apply();
        prefs.edit().remove(SESSION_NAME).apply();
        prefs.edit().remove(TOKEN).apply();
    }
}
package com.manyhands.checkup.old;

/**
 * Created by glassrb on 4/15/14.
 */
import java.sql.SQLException;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.manyhands.checkup.R;

public class FormGroupAdapter extends BaseAdapter {

    protected Context context;
    protected LayoutInflater inflater;
    protected FormGroup formGroup;

    public FormGroupAdapter(Context context, FormGroup formGroup) throws SQLException {
        this.context = context;
        this.formGroup = formGroup;

        inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return formGroup.getCount();
    }

    @Override
    public Object getItem(int position) {
        return formGroup.get(position);
    }

    @Override
    public long getItemId(int position) {
        return formGroup.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewGroup v = (ViewGroup) inflater.inflate((position%2==0? R.layout.list_item_eq:R.layout.list_item_uneq), parent, false);

        ((TextView)v.findViewById(R.id.list_view_item)).setText(formGroup.get(position).getTitle());

        return v;
    }

}

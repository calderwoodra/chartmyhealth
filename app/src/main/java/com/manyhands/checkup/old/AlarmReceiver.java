package com.manyhands.checkup.old;

/**
 * Created by me on 9/6/2014.
 */
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.media.MediaPlayer;
        import android.media.RingtoneManager;
        import android.net.Uri;
        import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Toast.makeText(context, "Alarm for Medication!", Toast.LENGTH_LONG).show();
        final MediaPlayer mp = MediaPlayer.create(context, notification);
        mp.start();
        ((DrupalLogin)context).showAlarmDialog(mp);
    }


}

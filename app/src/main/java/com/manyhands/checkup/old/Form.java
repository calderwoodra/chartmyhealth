package com.manyhands.checkup.old;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by me on 9/7/2014.
 */
public class Form {
    private List<Question> questions;
    private int nid;
    private String title;

    public Form(String text) {
        questions = new ArrayList<Question>();
        parseQuestions(text);
    }

    private void parseQuestions(String text) {
        text = text.substring(5);
        text = text.replace("\\", "");
        String[] questionSplit = text.split("nnnnnn");
        String[] labelSplit;
        String[] optionSplit;

        for (int i = 0; i < questionSplit.length - 2; i++) {
            labelSplit = questionSplit[i].split("n n");
            Question q = new Question(labelSplit[0].trim());
            optionSplit = labelSplit[1].split("nnnn");

            for (String option : optionSplit)
                q.addValue(option.trim());

            questions.add(q);
        }

        questionSplit[questionSplit.length - 1] = questionSplit[questionSplit.length - 1].substring(questionSplit[questionSplit.length - 1].indexOf("title") + 8);
        title = questionSplit[questionSplit.length - 1].substring(0,questionSplit[questionSplit.length - 1].indexOf(",") - 1);
        int x = questionSplit[questionSplit.length - 1].indexOf("Nid");
        nid = Integer.parseInt(questionSplit[questionSplit.length - 1].substring(x+6, questionSplit[questionSplit.length - 1].indexOf("}")-1));
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public class Question {
        public String label;
        public List<String> keys;
        public List<String> values;
        public int type = -1;
        public List<String> answers;

        public Question(String label) {
            this.label = label;

            keys = new ArrayList<String>();
            values = new ArrayList<String>();
            answers = new ArrayList<String>();
        }

        public void addValue(String value) {
            this.values.add(value);
            this.answers.add(0 + "");
        }

        public void setAnswer(int i, String value) {
            answers.set(i, value);
        }

        public void setValue(int i, String value) {
            values.set(i, value);
        }

        public String getValue(int i) {
            return values.get(i);
        }

        public int getValueSize() {
            return values.size();
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getLabel() {
            return label;
        }
    }

    public int getId() {
        return nid;
    }

    public String getTitle() {
        return title;
    }
}
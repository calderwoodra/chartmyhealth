package com.manyhands.checkup.old;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.JsonReader;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.manyhands.checkup.R;
import com.insready.drupalcloud.RESTServerClient;
import com.insready.drupalcloud.ServiceNotAvailableException;
import com.manyhands.checkup.activity.TestModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Scanner;


public class DrupalLogin extends Activity implements AdapterView.OnItemClickListener {
    private static final int CONTENT_VIEW_ID = 10101010;
    final static int RQS_1 = 1;

    TextView mText = null;
    Button linkButton = null;
    TextView checkupText = null;
    HttpClient httpClient = new DefaultHttpClient();
    HttpPost httpPost;
    FormGroup formGroup;
    FormFragment formFragment = null;
    String response;
    LinearLayout loadingStuff;
    Alarm alarm = new Alarm();

    boolean alarm2 = false;

    RESTServerClient getForms;
    RESTServerClient postForm;
    String userAccount;
    AlertDialog.Builder alert;
    Form form;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drupal_login);

        Intent i = new Intent(this, TestModel.class);
        startActivity(i);

        init();
    }

    public void init() {
        linkButton = ((Button)findViewById(R.id.button_link_account));
        checkupText = ((TextView)findViewById(R.id.textView_checkups));
        loadingStuff = ((LinearLayout)findViewById(R.id.loading_stuff));
        checkupText.setVisibility(View.INVISIBLE);
        httpPost = new HttpPost(getString(R.string.SERVER) + "checkup-retrieval");

        getForms = new RESTServerClient(
                getString(R.string.SERVER), getString(R.string.ENDPOINT));

        postForm = new RESTServerClient(
                getString(R.string.SERVER), getString(R.string.ENDPOINT));

        if(fileExists()) {
            load();
            new ServiceWrapper().execute(getForms);
            linkButton.setText("Unlink Checkup Account");
        }
    }

    public void linkAccountWithApp(View v) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Setup your account");
        alert.setMessage("Enter your email address to link your Checkup account.");

        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                userAccount = input.getText().toString();
                new ServiceWrapper().execute(getForms);
                linkButton.setText("Unlink Checkup Account");
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    protected void loadForms() {
        ListView lv = ((ListView) findViewById(R.id.forms_list));

        try {
            FormGroupAdapter adapter = new FormGroupAdapter(this, formGroup);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(this);

        } catch (SQLException e) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ((LinearLayout)findViewById(R.id.loading_stuff)).setVisibility(View.INVISIBLE);

        if(!alarm2)
            setAlarm();

        form = formGroup.get(position);
        FrameLayout frame = new FrameLayout(this);
        frame.setId(CONTENT_VIEW_ID);
        setContentView(frame, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        formFragment = new FormFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(CONTENT_VIEW_ID, formFragment).commit();
    }

    private class ServiceWrapper extends AsyncTask<RESTServerClient, Void, String> {
        @Override
        protected String doInBackground(RESTServerClient... rsc) {
            String session = "";
            String token = "";
            String result = "";

            try {

                JsonReader jsr = rsc[0].userLogin(userAccount, "asdf");
                jsr.beginObject();
                jsr.nextName();
                session = jsr.nextString();
                jsr.nextName();
                session = jsr.nextString() + "=" + session;
                jsr.nextName();
                token = jsr.nextString();
                rsc[0].setSession(session);
                rsc[0].setToken(token);

                HttpGet httpGet = new HttpGet(getString(R.string.SERVER) + "checkup-retrieval");
                httpGet.setHeader("Cookie", session);
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity entity = response.getEntity();


                //returned.
                result = convertStreamToString(entity.getContent());

                JSONObject json=new JSONObject(result);
                Log.i("Praeda","<jsonobject>\n"+json.toString()+"\n</jsonobject>");

                JSONArray nameArray = json.names();
                JSONArray valArray = json.toJSONArray(nameArray);

                formGroup = new FormGroup(valArray.getString(0));
                formGroup.toString();

            } catch (ServiceNotAvailableException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            return result;
        }

        protected void onPostExecute(String result) {
            write(userAccount);
            checkupText.setVisibility(View.VISIBLE);
            loadForms();

        }
    }

    private void setAlarm(){

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Toast.makeText(this, "Alarm for Medication!", Toast.LENGTH_LONG).show();
        final MediaPlayer mp = MediaPlayer.create(this, notification);
        mp.start();
        showAlarmDialog(mp);
        alarm2 = true;
    }

    public void showAlarmDialog(final MediaPlayer mp) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Take your medication and submit your checkup!")
                .setCancelable(false)
                .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mp.stop();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private class ServiceWrapper2 extends AsyncTask<RESTServerClient, Void, String> {
        @Override
        protected String doInBackground(RESTServerClient... rsc) {
            String session = "";
            String token = "";
            String result = "";

            try {



                JsonReader jsr = rsc[0].userLogin(userAccount, "asdf");

                jsr.beginObject();
                jsr.nextName();
                session = jsr.nextString();
                jsr.nextName();
                session = jsr.nextString() + "=" + session;
                jsr.nextName();
                token = jsr.nextString();
                rsc[0].setSession(session);
                rsc[0].setToken(token);

                HttpGet httpGet = new HttpGet(getString(R.string.SERVER) + "checkup-response" + response);
                httpGet.setHeader("Cookie", session);
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity entity = response.getEntity();

                result = convertStreamToString(entity.getContent());

                JSONObject json=new JSONObject(result);
                Log.i("Praeda","<jsonobject>\n"+json.toString()+"\n</jsonobject>");

                JSONArray nameArray = json.names();
                JSONArray valArray = json.toJSONArray(nameArray);

                formGroup = new FormGroup(valArray.getString(0));
                formGroup.toString();

            } catch (ServiceNotAvailableException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            return result;
        }

        protected void onPostExecute(String result) {
            checkupText.setVisibility(View.INVISIBLE);
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            loadingStuff.setVisibility(View.VISIBLE);
            ft.remove(formFragment).commit();
            setContentView(R.layout.activity_drupal_login);
            init();
        }
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the BufferedReader.readLine()
         * method. We iterate until the BufferedReader return null which means
         * there's no more data to read. Each line will appended to a StringBuilder
         * and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drupal_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean fileExists() {
        File sdCard = Environment.getExternalStorageDirectory();
        return new File(sdCard.getAbsolutePath() + "/checkup/userAccount.1").isFile();
    }

    public void load() {
        File sdCard = Environment.getExternalStorageDirectory();
        try {
            Scanner scan = new Scanner(new File(sdCard.getAbsolutePath() + "/checkup/userAccount.1"));
            userAccount = scan.next();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void write(String userAccount) {
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File (sdCard.getAbsolutePath() + "/checkup");
        dir.mkdirs();
        File file = new File(dir, "userAccount.1");

        try {
            FileOutputStream os = new FileOutputStream(file);
            os.write(userAccount.getBytes());
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Form getForm() {
        return form;
    }

    public void postResponse(String response) {
        this.response = response;
        new ServiceWrapper2().execute(postForm);
    }
}

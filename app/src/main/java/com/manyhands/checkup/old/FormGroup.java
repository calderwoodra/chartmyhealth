package com.manyhands.checkup.old;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by me on 9/7/2014.
 */
public class FormGroup {
    public static final int RADIO = 0;
    public static final int CHECKBOX = 1;

    private List<Form> forms;


    public FormGroup(String text) {
        forms = new ArrayList<Form>();
        parseforms(text);
    }

    private void parseforms(String text) {
        Log.d("S", text);
        String[] formSplit = text.split("Form");
        for (int j = 1; j < formSplit.length; j++) {
            forms.add(new Form(formSplit[j]));
        }
    }

    public List<Form> getForms() {
        return forms;
    }

    public int getCount() {
        return forms.size();
    }

    public Form get(int i) {
        return forms.get(i);
    }

    public long getItemId(int i) {
        return forms.get(i).getId();
    }
}

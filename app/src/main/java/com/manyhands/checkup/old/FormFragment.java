package com.manyhands.checkup.old;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.manyhands.checkup.R;

import java.util.List;

/**
 * Created by me on 7/18/2014.
 */
public class FormFragment extends Fragment implements View.OnClickListener {

    Form form;
    View rootView;
    List<Form.Question> questions;
    ViewGroup container;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       form = ((DrupalLogin) getActivity()).getForm();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.form_fragment, container, false);

        ((TextView)rootView.findViewById(R.id.textView_title)).setText("Checkup " + form.getTitle());

        questions = form.getQuestions();


        ScrollView sc = new ScrollView(rootView.getContext());
        LinearLayout ll = new LinearLayout(rootView.getContext());
        TextView tv1 = new TextView(rootView.getContext());
        tv1.setTextAppearance(rootView.getContext(), android.R.style.TextAppearance_Large);
        tv1.setText(" ");
        TextView tv2 = new TextView(rootView.getContext());
        tv2.setTextAppearance(rootView.getContext(), android.R.style.TextAppearance_Large);
        tv2.setText(" ");
        sc.addView(ll);

        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(tv1);
        ll.addView(tv2);
        int i = 101010102;
        for(Form.Question question : questions) {
            if(question.getValueSize() == 2) {
                TextView tv = new TextView(rootView.getContext());
                tv.setText(question.getLabel());
                RadioGroup radioGroup = new RadioGroup(rootView.getContext());
                radioGroup.setId(i);
                RadioButton rb1 = new RadioButton(rootView.getContext());
                rb1.setText("Yes");
                radioGroup.addView(rb1);
                RadioButton rb2 = new RadioButton(rootView.getContext());
                rb2.setText("No");
                radioGroup.addView(rb2);
                ll.addView(tv);
                ll.addView(radioGroup);
                i++;
            }
            else { //if(question.getValueSize() == 5) {
                TextView tv = new TextView(rootView.getContext());
                tv.setText(question.getLabel());
                RatingBar ratingBar = new RatingBar(rootView.getContext());
                ratingBar.setId(i);
                ratingBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                ratingBar.setRating(0f);
                ratingBar.setMax(question.getValueSize());
                ll.addView(tv);
                ll.addView(ratingBar);
                i++;
            }
        }

        Button button = new Button(rootView.getContext());
        button.setText("Submit");
        button.setId(101010101);
        button.setOnClickListener(this);
        ll.addView(button);
        container.addView(sc);

        this.rootView = rootView;
        this.container = container;
        return rootView;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case 101010101:
                String response = "?id="+form.getId()+"&";
                for(int i = 101010102; i < 101010102 + questions.size(); i++) {
                    Object object = container.findViewById(i);

                    if(object instanceof RadioGroup) {
                        RadioGroup rg = (RadioGroup)object;
                        if(((RadioButton)rg.getChildAt(0)).isChecked())
                            response+=(i+1-101010102)+"=1";
                        else
                            response+=(i+1-101010102)+"=2";
                    }
                    else if(object instanceof RatingBar) {
                        response+=(i+1-101010102)+"="+Math.round(((RatingBar)object).getRating());
                    }
                    response += "&";
                }

                ((DrupalLogin) getActivity()).postResponse(response);
                break;
            default:
                break;
        }
    }
}
/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonReader;

import com.insready.drupalcloud.RESTServerClient;
import com.insready.drupalcloud.ServiceNotAvailableException;
import com.manyhands.checkup.activity.ActivityMain;
import com.manyhands.checkup.model.HealthPlan;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by me on 10/25/2014.
 */
public final class Network {
    //SERVER PROTOCOL CONSTANTS
    public static final String SERVER_URL = "http://server2.manyhandscollective.com/hacknc/";
    public static final String SERVER_ENDPOINT = "android/";
    public static final String INVITE_CODE = "invite";

    //PROTOCOL MESSAGE CONSTANTS
    public static final String REGISTER = "Registering Account";
    public static final String LOGIN = "Logging In";
    public static final String LOGOUT = "Logging Out";

    private static final RESTServerClient restServerClient;
    private static Context context;

    static {
        restServerClient = new RESTServerClient(SERVER_URL, SERVER_ENDPOINT);
    }

    public static void setContext(Context c){
        context = c;
    }

    public static void setSession(Session session) {
        restServerClient.setSession(session.getSessionString());
        restServerClient.setToken(session.getToken());
    }

    public static ProgressDialog getProgressDialog(String title) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle(title);
        progressDialog.setCancelable(false);
        progressDialog.show();

        return progressDialog;
    }

    public static Session register(final String email, final String password, final String accessCode){
        ProgressDialog progressDialog = getProgressDialog(REGISTER);

        AsyncTask registerTask = new AsyncTask<Object, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Object...objects) {
                Session session = new Session();
                try {
                    JsonReader jsr = restServerClient.userRegister(email.toLowerCase(), password, email.toLowerCase());

                    if(jsr != null) {
                        return true;
                    }
                    /*jsr.beginObject();
                    jsr.nextName();
                    session.setSessionName(jsr.nextString());
                    jsr.nextName();
                    session.setSessionId(jsr.nextString());
                    jsr.nextName();
                    session.setToken(jsr.nextString());
                    restServerClient.setSession(session.getSessionString());
                    restServerClient.setToken(session.getToken());*/
                } catch (ServiceNotAvailableException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return false;
            }
        };

        try {
            boolean registered = (Boolean) registerTask.execute().get();
            Session session = new Session();
            if(registered)
                session = login(email, password);

            if(session.isSet())
                return session;
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        finally {
            progressDialog.dismiss();
        }

        return null;
    }

    public static Session login(final String email, final String password) {
        ProgressDialog progressDialog = getProgressDialog(LOGIN);
        final Session session = new Session();

        AsyncTask loginTask = new AsyncTask<Object, Void, Void>() {
            @Override
            protected Void doInBackground(Object...objects) {
                try {
                    JsonReader jsr = restServerClient.userLogin(email.toLowerCase(), password);
                    jsr.beginObject();
                    jsr.nextName();
                    session.setSessionName(jsr.nextString());
                    jsr.nextName();
                    session.setSessionId(jsr.nextString());
                    jsr.nextName();
                    session.setToken(jsr.nextString());
                    setSession(session);
                } catch (ServiceNotAvailableException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return null;
            }
        };

        try {

            loginTask.execute().get();

            if(session.isSet())
                return session;
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        finally {
            progressDialog.dismiss();
        }

        return null;
    }

    public static boolean logout(final Session session) {
        final ProgressDialog progressDialog = getProgressDialog(LOGOUT);

        AsyncTask logoutTask = new AsyncTask<Object, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Object...objects) {
                try {
                    restServerClient.userLogout(session.getSessionId());
                    return true;
                }
                catch (ServiceNotAvailableException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                return false;
            }
        };

        try {
            return (Boolean) logoutTask.execute().get();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        catch (ExecutionException e) {
            e.printStackTrace();
        }
        finally {
            progressDialog.dismiss();
        }

        return false;
    }

    public static List<HealthPlan> updateHealthPlans(Session session) {
        return null;
    }

    public static boolean submit() {
        return true;
    }
}

/*
 * Copyright (c) 2014. MANYHANDS Collective LLC
 */

package com.manyhands.checkup.network;

/**
 * Created by me on 10/25/2014.
 */
public class Session {
    private String sessionId;
    private String sessionName;
    private String token;

    public Session() {
        this("","","");
    }

    public Session(String sessionId, String sessionName, String token) {
        this.sessionId = sessionId;
        this.sessionName = sessionName;
        this.token = token;
    }

    public String getSessionString() {
        return sessionName+"="+sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isSet() {
        if(sessionId.length() > 0 && sessionName.length() > 0 && token.length() > 0)
            return true;

        return false;
    }

    @Override
    public String toString() {
        return "Session{" +
                "sessionId='" + sessionId + '\'' +
                ", sessionName='" + sessionName + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}

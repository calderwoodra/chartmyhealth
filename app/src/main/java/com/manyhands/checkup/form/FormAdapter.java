package com.manyhands.checkup.form;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.manyhands.checkup.R;
import com.manyhands.checkup.model.Form;

import java.sql.SQLException;

public class FormAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private Form form;

    public FormAdapter(Context context, Form form) throws SQLException {
        this.context = context;
        this.form = form;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return form.size();
    }

    @Override
    public Object getItem(int position) {
        //return form.getElement(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return form.getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewGroup v = (ViewGroup) inflater.inflate((position%2==0?R.layout.list_item_eq:R.layout.list_item_uneq), parent, false);

        //((TextView)v.findViewById(R.id.list_view_item)).setText(form.get(position).getTitle());

        return v;
    }
}

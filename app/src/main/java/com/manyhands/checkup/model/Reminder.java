package com.manyhands.checkup.model;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;

import java.sql.SQLException;

@DatabaseTable(tableName = Reminder.KEY)
public class Reminder extends BaseDaoEnabled<Reminder, Integer> {
    public static final String KEY = "reminder";

    @DatabaseField(generatedId = true)
    protected int id = 0;
    @DatabaseField
    protected String uuid;
    @DatabaseField
    protected String description;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    protected HealthPlan healthPlan;

    private static Dao<Reminder, Integer> reminderDao;

    public Reminder() {}

    public Reminder(HealthPlan healthPlan, String uuid, String description) {
        this.uuid = uuid;
        this.healthPlan = healthPlan;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public HealthPlan getHealthPlan() {
        return healthPlan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Reminder{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", description='" + description + '\'' +
                ", healthPlan=" + healthPlan +
                '}';
    }

    public static Reminder create(Context context, HealthPlan healthPlan, String uuid, String description) {
        try {
            if(reminderDao == null)
                reminderDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Reminder.class);

            Reminder reminder = new Reminder(healthPlan, uuid, description);
            reminderDao.createOrUpdate(reminder);
            return reminder;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

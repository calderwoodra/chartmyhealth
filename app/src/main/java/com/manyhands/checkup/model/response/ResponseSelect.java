package com.manyhands.checkup.model.response;

import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Response;
import com.manyhands.checkup.model.question.QuestionSelect;

public class ResponseSelect extends Response {

    public ResponseSelect(Question question, int selected) {
        super(question, selected);
    }

    public String getSelectedText() {
        return ((QuestionSelect) question).getOptions()[getSelected()];
    }

    public int getSelected() {
        return data.getIntData();
    }

    public String toString() {
        return super.toString() + " " + getSelected() + " " + getSelectedText();
    }
}

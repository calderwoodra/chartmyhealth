package com.manyhands.checkup.model;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;

import java.sql.SQLException;

@DatabaseTable(tableName = Biometric.KEY)
public class Biometric extends BaseDaoEnabled<Biometric, Integer> {
    public static final String KEY = "biometric";
    public enum Type { PULSE, TEMPERATURE}

    @DatabaseField(generatedId = true)
    protected int id = 0;
    @DatabaseField
    protected Type type;
    @DatabaseField
    protected long timestamp;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected float[] data;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    protected HealthPlan healthPlan;

    private static Dao<Biometric, Integer> biometricDao;

    public Biometric() {}

    public Biometric(HealthPlan healthPlan, Type type, float...data) {
        this.healthPlan = healthPlan;
        this.type = type;
        this.data = data;

        timestamp = System.currentTimeMillis();
    }

    public int getId() {
        return id;
    }

    public HealthPlan getHealthPlan() {
        return healthPlan;
    }

    public Type getType() {
        return type;
    }

    public int size() {
        return data.length;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(type).append(" ").append(timestamp).append(" [");

        for(int i = 0; i < data.length - 1; i++)
            sb.append(data[i]).append(", ");
        sb.append(data[data.length - 1]).append("]");

        return sb.toString();
    }

    public static Biometric create(Context context, HealthPlan healthPlan, Type type, float...data) {
        try {
            if(biometricDao == null)
                biometricDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Biometric.class);

            Biometric biometric = new Biometric(healthPlan, type, data);
            biometricDao.createOrUpdate(biometric);
            healthPlan.addBiometric(biometric).update();
            return biometric;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

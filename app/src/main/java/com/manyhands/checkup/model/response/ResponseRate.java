package com.manyhands.checkup.model.response;

import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Response;

public class ResponseRate extends Response {

    public ResponseRate(Question question, float rating) {
        super(question, rating);
    }

    public float getRating() {
        return data.getFloatData();
    }

    public void setRating(float rating) {
        data.setFloatData(rating);
    }

    public String toString() {
        return super.toString() + " " + getRating();
    }
}

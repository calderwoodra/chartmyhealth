package com.manyhands.checkup.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.response.ResponseCheckbox;
import com.manyhands.checkup.model.response.ResponseRate;
import com.manyhands.checkup.model.response.ResponseSelect;
import com.manyhands.checkup.model.response.ResponseSort;

@DatabaseTable(tableName = Response.KEY)
abstract public class Response extends BaseDaoEnabled<Response, Integer> {
    public static final String KEY = "response";

    @DatabaseField(generatedId = true)
    protected int id = 0;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected DataObject data;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    protected Question question;

    public Response() {}

    public Response(Question question, int...data) {
        this.question = question;
        this.data = new DataObject(data);
    }

    public Response(Question question, float...data) {
        this.question = question;
        this.data = new DataObject(data);
    }

    public int getId() {
        return id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Question.Type getType() {
        return question.getType();
    }

    public Object getData() {
        return data;
    }

    public String toString() {
        return question.toString();
    }

    public static Response create(Question question, int...data){
        if(question == null)
            return null;

        switch (question.getType()) {
            case SELECT:
                return new ResponseSelect(question, data[0]);
            case CHECKBOX:
                return new ResponseCheckbox(question, data);
            case SORT:
                return new ResponseSort(question, data);
        }

        return null;
    }

    public static Response create(Question question, float...data){
        if(question == null)
            return null;

        switch (question.getType()) {
            case RATE:
                return new ResponseRate(question, data[0]);
        }

        return null;
    }
}

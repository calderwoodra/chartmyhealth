package com.manyhands.checkup.model.question;

import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.Question;

public class QuestionSort extends Question {
    public QuestionSort(Form form, String uuid, String text, String[] options) {
        super(form, uuid, Type.SORT, text, options);
    }

    public final String[] getOptions() {
        return data.getStringArrayData();
    }

    public String getOption(int i) {
        return data.getStringArrayData()[i];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String[] d = data.getStringArrayData();
        sb.append(super.toString()).append(" ");
        for(int i = 0; i < d.length - 1; i++)
            sb.append(d[i]).append(", ");
        sb.append(d[d.length - 1]);

        return sb.toString();
    }
}

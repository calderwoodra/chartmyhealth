package com.manyhands.checkup.model;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;

import java.sql.SQLException;
import java.util.Comparator;

@DatabaseTable(tableName = ScheduledEvent.KEY)
public class ScheduledEvent {
    public static final String KEY = "scheduled_event";
    public static enum Type { FORM, REMINDER, BIOMETRIC }


    private static final ActivateTimeComparator ATC = new ActivateTimeComparator();

    @DatabaseField(generatedId = true)
    private int id = 0;
    @DatabaseField
    private String uuid;
    @DatabaseField
    private Type type;
    @DatabaseField
    private long startTime;
    @DatabaseField
    private long endTime;
    @DatabaseField
    private long interval;
    @DatabaseField
    private long intervalCount;
    @DatabaseField
    private boolean repeats;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    private Schedule schedule;

    private static Dao<ScheduledEvent, Integer> scheduledEventDao;

    public ScheduledEvent() {}

    public ScheduledEvent(Schedule schedule, String uuid, Type type, long time) {
        this.schedule = schedule;
        this.uuid = uuid;
        this.type = type;
        this.startTime = this.endTime = this.interval = time;
        repeats = false;
    }

    public ScheduledEvent(Schedule schedule, String uuid, Type type, long startTime, long interval) {
        this.schedule = schedule;
        this.uuid = uuid;
        this.type = type;
        this.startTime = startTime;
        this.endTime = Long.MAX_VALUE;
        this.interval = interval;
        repeats = true;
    }

    public ScheduledEvent(Schedule schedule, String uuid, Type type, long startTime, long interval, long endTime) {
        this.schedule = schedule;
        this.uuid = uuid;
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
        this.interval = interval;
        repeats = true;
    }

    // only called in method getNextEvent()
    private ScheduledEvent(ScheduledEvent event, long intervalCount) {
        this(event.getSchedule(), event.getUuid(), event.getType(), event.getStartTime(), event.getInterval(), event.getEndTime());
        this.intervalCount = intervalCount;
        repeats = true;
    }

    public int getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }

    // creates a non-repeating event based on a repeating ScheduledEvent
    // used for sorting in the Scheduler
    public ScheduledEvent getNextEvent()
    {
        return new ScheduledEvent(this, intervalCount++);
    }

    public long getActivateTime() {
        return startTime + (interval * intervalCount);
    }

    public long getNextActivateTime() {
        return startTime + (interval * (intervalCount + 1));
    }

    public void scheduled()
    {
        intervalCount++;
    }

    public boolean isSchedulable()
    {
        //if(getActivateTime() < System.currentTimeMillis())
        //    return false; // already happened, so cannot schedule
        if(!isRepeating() && intervalCount == 0)
            return true;

        if(getNextActivateTime() < endTime)
            return true;
        else
            return false; // next activate time
    }

    public boolean isRepeating() {
        return repeats;
    }

    /*@Override
    public String toString() {
        return "ScheduledEvent{" +
                "uuid='" + uuid + '\'' +
                ", type=" + type +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", interval=" + interval +
                ", repeats=" + isRepeating() +
                ", time=" + getActivateTime() +
                ", count=" + intervalCount +
                '}';
    }*/

    @Override
    public String toString() {
        return "ScheduledEvent{" +
                "uuid=" + uuid +
                ", repeats=" + repeats +
                ", time=" + getActivateTime() +
                ", count=" + intervalCount +
                '}';
    }

    public static ActivateTimeComparator getTimeNonRepeatComparator()
    {
        return ATC;
    }

    private static final class ActivateTimeComparator implements Comparator<ScheduledEvent> {
        public int compare(ScheduledEvent s1, ScheduledEvent s2) {
            if(s1.getActivateTime() > s2.getActivateTime())
                return 1;
            else if(s1.getActivateTime() < s2.getActivateTime())
                return -1;
            else
                return 0;
        }
    }

    public static ScheduledEvent create(Context context, Schedule schedule, String uuid, Type type, long time) {
        try {
            if(scheduledEventDao == null)
                scheduledEventDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(ScheduledEvent.class);

            ScheduledEvent scheduledEvent = new ScheduledEvent(schedule, uuid, type, time);
            scheduledEventDao.createOrUpdate(scheduledEvent);
            return scheduledEvent;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ScheduledEvent create(Context context, Schedule schedule, String uuid, Type type, long begin, long interval) {
        try {
            if(scheduledEventDao == null)
                scheduledEventDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(ScheduledEvent.class);

            ScheduledEvent scheduledEvent = new ScheduledEvent(schedule, uuid, type, begin, interval);
            scheduledEventDao.createOrUpdate(scheduledEvent);
            return scheduledEvent;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ScheduledEvent create(Context context, Schedule schedule, String uuid, Type type, long begin, long interval, long end) {
        try {
            if(scheduledEventDao == null)
                scheduledEventDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(ScheduledEvent.class);

            ScheduledEvent scheduledEvent = new ScheduledEvent(schedule, uuid, type, begin, interval, end);
            scheduledEventDao.createOrUpdate(scheduledEvent);
            schedule.addScheduledEvent(scheduledEvent).update();
            return scheduledEvent;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

package com.manyhands.checkup.model.question;

import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.Question;

public class QuestionRate extends Question {
    public static final int MIN = 0;
    public static final int MAX = 1;

    private int selectedRate;

    public QuestionRate(Form form, String uuid, String text, int min, int max) {
        super(form, uuid, Type.RATE, text, min, max);
    }

    public void setSelectedRate(int rate){
        selectedRate = rate;
    }

    public int getMin() {
        return data.getIntArrayData()[MIN];
    }

    public int getMax() {
        return data.getIntArrayData()[MAX];
    }

    public String toString() {
        int[] minMax = data.getIntArrayData();

        return super.toString() + " " + minMax[MIN] + " " + minMax[MAX];
    }
}

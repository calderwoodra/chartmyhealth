package com.manyhands.checkup.model;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = HealthPlan.KEY)
public class HealthPlan extends BaseDaoEnabled<HealthPlan, Integer> {
    public static final String KEY = "health_plan";

    @DatabaseField(generatedId = true)
    protected int id;
    @DatabaseField
    protected String name;
    @DatabaseField
    protected String description;
    @DatabaseField
    protected String uuid;
    @DatabaseField
    protected boolean enabled = true;
    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    private Schedule schedule;

    @ForeignCollectionField
    protected Collection<Form> forms;
    @ForeignCollectionField
    protected Collection<Reminder> reminders;
    @ForeignCollectionField
    protected Collection<Biometric> biometrics;

    private static Dao<HealthPlan, Integer> healthPlanDao;

    public HealthPlan() {}

    protected HealthPlan(String uuid, String name, String description) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        forms = new ArrayList<Form>();
        reminders = new ArrayList<Reminder>();
        biometrics = new ArrayList<Biometric>();
    }

    public HealthPlan(String uuid, String name, String description,
                      Schedule schedule, Collection<Form> forms, Collection<Reminder> reminders) {
        this.uuid = uuid;
        this.name = name;
        this.description = description;
        this.schedule = schedule;
        this.forms = forms;
        this.reminders = reminders;
    }

    public int getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public HealthPlan setSchedule(Schedule schedule) {
        this.schedule = schedule;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void enable() {
        enabled = true;
    }

    public void disable() {
        enabled = false;
    }

    public Form[] getForms() {
        return forms.toArray(new Form[forms.size()]);
    }

    public void setForms(Collection<Form> forms) {
        this.forms = forms;
    }

    public HealthPlan addForm(Form form) {
        forms.add(form);
        return this;
    }

    public Collection<Reminder> getReminders() {
        return reminders;
    }

    public Biometric[] getBiometrics() {
        return biometrics.toArray(new Biometric[biometrics.size()]);
    }

    public void setBiometrics(Collection<Biometric> biometrics) {
        this.biometrics = biometrics;
    }

    public HealthPlan addBiometric(Biometric biometric) {
        biometrics.add(biometric);
        return this;
    }

    public String toString() {
        return uuid + " " + name + " "
                + Arrays.toString(forms.toArray())
                + Arrays.toString(reminders.toArray());
    }

    public HealthPlan save(Context context) {
        try {
            Dao<HealthPlan, Integer> healthPlanDao =
                    OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(HealthPlan.class);
            healthPlanDao.createOrUpdate(this);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public static List<HealthPlan> loadAll(Context context) {
        try {
            if(healthPlanDao == null)
                healthPlanDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(HealthPlan.class);

            return healthPlanDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static HealthPlan loadByUuid(Context context, String uuid) {
        try {
            if(healthPlanDao == null)
                healthPlanDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(HealthPlan.class);

            return healthPlanDao.queryForFirst(healthPlanDao.queryBuilder().where().eq("uuid", uuid).prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Collection<HealthPlan> loadByEnabled(Context context, boolean enabled) {
        try {
            if(healthPlanDao == null)
                healthPlanDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(HealthPlan.class);

            return healthPlanDao.query(healthPlanDao.queryBuilder().where().eq("enabled", enabled).prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static HealthPlan create(Context context, String uuid, String name, String description){
        try {
            if(healthPlanDao == null)
                healthPlanDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(HealthPlan.class);

            HealthPlan healthPlan = new HealthPlan(uuid, name, description);
            healthPlanDao.createOrUpdate(healthPlan);
            return healthPlan;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

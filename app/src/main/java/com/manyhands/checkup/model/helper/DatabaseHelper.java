package com.manyhands.checkup.model.helper;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.manyhands.checkup.model.Biometric;
import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.HealthPlan;
import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Reminder;
import com.manyhands.checkup.model.Response;
import com.manyhands.checkup.model.Schedule;
import com.manyhands.checkup.model.ScheduledEvent;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    public static final String DATABASE_NAME = "checkup.db";

    private static final Class<?>[] ormClasses =
            { Question.class, Response.class, Biometric.class, HealthPlan.class, Reminder.class, Form.class, Schedule.class, ScheduledEvent.class };
    private static final int DATABASE_VERSION = 32; //change for each database upgrade

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            for (Class<?> ormClass : ormClasses) {
                TableUtils.createTable(this.getConnectionSource(), ormClass);
                Log.d("DatabaseHelper", ormClass.getName() + " created");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            try {
                switch(oldVersion){

                    default:
                        for (Class<?> ormClass : ormClasses) {
                            TableUtils.dropTable(this.getConnectionSource(), ormClass, true);
                        }

                        for (Class<?> ormClass : ormClasses) {
                            TableUtils.createTable(this.getConnectionSource(), ormClass);
                        }
                        break;
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

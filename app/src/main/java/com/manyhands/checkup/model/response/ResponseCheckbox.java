package com.manyhands.checkup.model.response;

import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Response;
import com.manyhands.checkup.model.question.QuestionCheckbox;

import java.util.Arrays;

public class ResponseCheckbox extends Response {

    public ResponseCheckbox(Question question, int[] selected) {
        super(question, selected);
    }

    public String[] getSelectedText() {
        final String[] options = ((QuestionCheckbox) question).getOptions();
        final int[] selected = getSelected();
        String[] selectedText = new String[selected.length];

        for(int i = 0; i < options.length; i++)
            selectedText[i] = options[selected[i]];

        return selectedText;
    }

    public int[] getSelected() {
        return  data.getIntArrayData();
    }

    public void setSelected(int[] selected) {
        data.setIntArrayData(selected);
    }

    public String toString() {
        return super.toString() + " " + Arrays.toString(getSelected()) + " " + Arrays.toString(getSelectedText());
    }
}


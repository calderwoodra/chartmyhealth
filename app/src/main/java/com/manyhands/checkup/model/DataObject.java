package com.manyhands.checkup.model;

import java.io.Serializable;

public class DataObject implements Serializable {
    private int intData;
    private int[] intArrayData;
    private float floatData;
    private float[] floatArrayData;
    private String stringData;
    private String[] stringArrayData;

    public DataObject(int intData) {
        this.intData = intData;
    }

    public DataObject(int[] intArrayData) {
        this.intArrayData = intArrayData;
    }

    public DataObject(float floatData) {
        this.floatData = floatData;
    }

    public DataObject(float[] floatArrayData) {
        this.floatArrayData = floatArrayData;
    }

    public DataObject(String stringData) {
        this.stringData = stringData;
    }

    public DataObject(String[] stringArrayData) {
        this.stringArrayData = stringArrayData;
    }

    public int getIntData() {
        return intData;
    }

    public void setIntData(int intData) {
        this.intData = intData;
    }

    public int[] getIntArrayData() {
        return intArrayData;
    }

    public void setIntArrayData(int[] intArrayData) {
        this.intArrayData = intArrayData;
    }

    public float getFloatData() {
        return floatData;
    }

    public void setFloatData(float floatData) {
        this.floatData = floatData;
    }

    public float[] getFloatArrayData() {
        return floatArrayData;
    }

    public void setFloatArrayData(float[] floatArrayData) {
        this.floatArrayData = floatArrayData;
    }

    public String getStringData() {
        return stringData;
    }

    public void setStringData(String stringData) {
        this.stringData = stringData;
    }

    public String[] getStringArrayData() {
        return stringArrayData;
    }

    public void setStringArrayData(String[] stringArrayData) {
        this.stringArrayData = stringArrayData;
    }
}

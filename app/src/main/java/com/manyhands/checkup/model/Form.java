package com.manyhands.checkup.model;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

@DatabaseTable(tableName = Form.KEY)
public class Form extends BaseDaoEnabled<Form, Integer> {
    public interface FormElement {    }

    public static final String KEY = "form";

    @DatabaseField(generatedId = true)
    protected int id = 0;
    @DatabaseField
    protected String uuid;
    @DatabaseField
    protected String name;
    @DatabaseField
    protected String submitText;
    @ForeignCollectionField
    protected Collection<Question> questions;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    protected HealthPlan healthPlan;

    public Form() {}

    protected Form(HealthPlan healthPlan, String uuid, String name, String submitText) {
        this.healthPlan = healthPlan;
        this.uuid = uuid;
        this.name = name;
        this.submitText = submitText;
        questions = new ArrayList<Question>();
    }

    public Form(String uuid, String name, String submitText, Collection<Question> questions) {
        this.uuid = uuid;
        this.name = name;
        this.submitText = submitText;
        this.questions = questions;
    }

    public int getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getSubmitText() {
        return submitText;
    }

    public HealthPlan getHealthPlan() {
        return healthPlan;
    }

    public final Question[] getQuestions() {
        return questions.toArray(new Question[questions.size()]);
    }

    public final Collection<Question> getQuestionsAL() {
        //ArrayList<Question> q = (ArrayList<Question>) questions;
        //Log.d("a", "" + questions.size());

        //q.addAll(questions);
        return questions;
    }

//    public final ArrayList<Question> getQuestionsAL(){
//        ArrayList<Question> questions1 = new ArrayList<Question>();
//        for(Question quest : questions){
//            questions1.add(quest);
//        }
//        return questions1;
//    }

    public Question getQuestion(int i) {
        return ((Question[]) questions.toArray())[i];
    }

    public Form addQuestion(Question question) {
        questions.add(question);
        return this;
    }

    public int size() {
        return questions.size();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(uuid).append(" ").append(name).append("\n");
        for(Question question : questions)
            sb.append(question).append("\n");

        return sb.toString();
    }

    public Form save(Context context) {
        try {
            Dao<Form, Integer> formDao =
                    OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Form.class);
            formDao.createOrUpdate(this);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public static Form loadByUuid(Context context, String uuid) {
        try {
            Dao<Form, Integer> formDao =
                    OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Form.class);

            Form form = formDao.queryForFirst(formDao.queryBuilder().where().eq("uuid", uuid).prepare());
            return form;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Form create(Context context, HealthPlan healthPlan, String uuid, String name, String submitText){
        try {
            Dao<Form, Integer> formDao =
                    OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Form.class);

            Form form = new Form(healthPlan, uuid, name, submitText);
            formDao.createOrUpdate(form);
            healthPlan.addForm(form).update();
            return form;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

package com.manyhands.checkup.model;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;
import com.manyhands.checkup.model.question.QuestionCheckbox;
import com.manyhands.checkup.model.question.QuestionRate;
import com.manyhands.checkup.model.question.QuestionSelect;
import com.manyhands.checkup.model.question.QuestionSort;

import java.sql.SQLException;

@DatabaseTable(tableName = Question.KEY)
abstract public class Question extends BaseDaoEnabled<Question, Integer> implements Form.FormElement {
    public static final String KEY = "question";
    public static enum Type {RATE, SELECT, CHECKBOX, SORT}

    @DatabaseField(generatedId = true)
    protected int id = 0;
    @DatabaseField
    protected String uuid;
    @DatabaseField
    protected Type type;
    @DatabaseField
    protected String text;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected DataObject data;

    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    protected Form form;

    protected static Dao<Question, Integer> questionDao;

    public Question() {}

    public Question(String uuid, Type type, String text, String...data) {
        this.uuid = uuid;
        this.type = type;
        this.text = text;
        this.data = new DataObject(data);
    }

    public Question(Form form, String uuid, Type type, String text, String...data) {
        this.form = form;
        this.uuid = uuid;
        this.type = type;
        this.text = text;
        this.data = new DataObject(data);
    }

    public Question(Form form, String uuid, Type type, String text, int...data) {
        this.form = form;
        this.uuid = uuid;
        this.type = type;
        this.text = text;
        this.data = new DataObject(data);
    }

    public Question(String uuid, Type type, String text, int...data) {
        this.uuid = uuid;
        this.type = type;
        this.text = text;
        this.data = new DataObject(data);
    }

    public int getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public Type getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object getData() {
        return data;
    }

    public String toString() {
        return uuid + " " + type + " " + text;
    }

    public Question save(Context context) {
        try {
            if(questionDao == null)
                questionDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Question.class);

            questionDao.createOrUpdate(this);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return this;
    }

    public static Question createCheckbox(Context context, Form form, String uuid, String text, String...data) {
        return create(context, form, uuid, Type.CHECKBOX, text, data);
    }

    public static Question createRate(Context context, Form form, String uuid, String text, int...data) {
        return create(context, form, uuid, Type.RATE, text, data);
    }

    public static Question createSelect(Context context, Form form, String uuid, String text, String...data) {
        return create(context, form, uuid, Type.SELECT, text, data);
    }

    public static Question createSort(Context context, Form form, String uuid, String text, String...data) {
        return create(context, form, uuid, Type.SORT, text, data);
    }

    public static Question create(Context context, Form form, String uuid, Type type, String text, String...data){
        try {
            Dao<Question, Integer> questionDao =
                    OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Question.class);

            Question question = null;
            switch (type) {
                case SELECT:
                    question = new QuestionSelect(form, uuid, text, (String[])data);
                    break;
                case CHECKBOX:
                    question = new QuestionCheckbox(form, uuid, text, (String[])data);
                    break;
                case SORT:
                    question = new QuestionSort(form, uuid, text, (String[])data);
                    break;
                default:
                    return null;
            }

            questionDao.createOrUpdate(question);
            form.addQuestion(question).update();
            return question;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Question create(Context context, Form form, String uuid, Type type, String text, int...data){
        try {
            Dao<Question, Integer> questionDao =
                    OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Question.class);

            Question question = null;
            switch (type) {
                case RATE:
                    question = new QuestionRate(form, uuid, text, data[0], data[1]);
                    break;
                default:
                    return null;
            }

            questionDao.createOrUpdate(question);
            form.addQuestion(question).update();
            return question;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

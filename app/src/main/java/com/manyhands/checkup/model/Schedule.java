package com.manyhands.checkup.model;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.table.DatabaseTable;
import com.manyhands.checkup.model.helper.DatabaseHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DatabaseTable(tableName = Schedule.KEY)
public class Schedule extends BaseDaoEnabled<Schedule, Integer> {
    public static final String KEY = "schedule";

    @DatabaseField(generatedId = true)
    private int id = 0;
    @DatabaseField
    private String uuid;
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private HealthPlan healthPlan;
    @ForeignCollectionField
    private Collection<ScheduledEvent> events;

    private static Dao<Schedule, Integer> scheduledDao;

    public Schedule() { }

    public Schedule(HealthPlan healthPlan, String uuid) {
        this(healthPlan, uuid, new ArrayList<ScheduledEvent>());
    }

    public Schedule(HealthPlan healthPlan, String uuid, List<ScheduledEvent> events) {
        this.healthPlan = healthPlan;
        this.uuid = uuid;
        this.events = events;
    }

    public int getId() {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public HealthPlan getHealthPlan() {
        return healthPlan;
    }

    public ArrayList<ScheduledEvent> getScheduledEvents() {
        ArrayList<ScheduledEvent> toReturn = new ArrayList<ScheduledEvent>(events.size());
        toReturn.addAll(events);
        return toReturn;
    }

    public ScheduledEvent getScheduledEvent(int i) {
        return events.toArray(new ScheduledEvent[events.size()])[i]; // this is terribly inefficient, because it create a new array
    }

    public Schedule addScheduledEvent(ScheduledEvent event) {
        events.add(event);
        return this;
    }

    public static Schedule create(Context context, HealthPlan healthPlan, String uuid) {
        try {
            if (scheduledDao == null)
                scheduledDao = OpenHelperManager.getHelper(context, DatabaseHelper.class).getDao(Schedule.class);

            Schedule schedule = new Schedule(healthPlan, uuid);
            scheduledDao.createOrUpdate(schedule);
            healthPlan.setSchedule(schedule).update();
            return schedule;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}

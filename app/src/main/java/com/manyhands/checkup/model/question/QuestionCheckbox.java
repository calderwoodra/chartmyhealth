package com.manyhands.checkup.model.question;

import com.manyhands.checkup.model.Form;
import com.manyhands.checkup.model.Question;

import java.util.ArrayList;

public class QuestionCheckbox extends Question {

    private ArrayList<String> selectedOptions;

    public QuestionCheckbox(Form form, String uuid, String text, String[] options) {
        super(form, uuid, Type.CHECKBOX, text, options);
        selectedOptions = new ArrayList<String>();
    }

    public void addOptionSelected(String option){
        selectedOptions.add(option);
    }

    public void removeOptionSelected(String option){
        selectedOptions.remove(option);
    }

    public final String[] getOptions() {
        return data.getStringArrayData();
    }

    public String getOption(int i) {
        return data.getStringArrayData()[i];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        String[] d = data.getStringArrayData();
        sb.append(super.toString()).append(" ");
        for(int i = 0; i < d.length - 1; i++)
            sb.append(d[i]).append(", ");
        sb.append(d[d.length - 1]);

        return sb.toString();
    }
}
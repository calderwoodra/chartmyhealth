package com.manyhands.checkup.model.response;

import com.manyhands.checkup.model.Question;
import com.manyhands.checkup.model.Response;
import com.manyhands.checkup.model.question.QuestionSort;

import java.util.Arrays;

public class ResponseSort extends Response {
    public ResponseSort(Question question, int[] order) {
        super(question, order);
    }

    public String[] getOrderText() {
        final String[] options = ((QuestionSort) question).getOptions();
        final int[] order = getOrder();
        String[] sortedOrder = new String[options.length];

        for(int i = 0; i < options.length; i++)
            sortedOrder[i] = options[order[i]];

        return sortedOrder;
    }

    public int[] getOrder() {
        return data.getIntArrayData();
    }

    public String toString() {
        return super.toString() + " " + Arrays.toString(getOrder()) + " " + Arrays.toString(getOrderText());
    }
}

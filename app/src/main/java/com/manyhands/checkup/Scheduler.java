package com.manyhands.checkup;

import android.app.Service;
import android.content.Context;

import com.manyhands.checkup.model.HealthPlan;
import com.manyhands.checkup.model.Schedule;
import com.manyhands.checkup.model.ScheduledEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;

public class Scheduler  {
    public static final long MILLISECONDS_IN_SECONDS = 1000;
    public static final long SECONDS_IN_MINUTES = 60;

    private static LinkedList<Schedule> schedules = new LinkedList<Schedule>();
    private static ArrayList<ScheduledEvent> events = new ArrayList<ScheduledEvent>();

    public static void loadSchedules(Context context){
        Collection<HealthPlan> healthPlans = HealthPlan.loadByEnabled(context, true);

        schedules.clear();
        for(HealthPlan healthPlan : healthPlans) {
            schedules.add(healthPlan.getSchedule());
        }
    }

    // populates the "schedules" queue up to n = @param(size)
    public static void populate(int size)
    {
        ArrayList<ScheduledEvent> repeats = new ArrayList<ScheduledEvent>(size);
        ListIterator<Schedule> iterator = schedules.listIterator();

        while(iterator.hasNext()){
            ArrayList<ScheduledEvent> schedule = iterator.next().getScheduledEvents();
            for(int j = 0; j < schedule.size(); j++)
            {
                ScheduledEvent event = schedule.get(j);

                if(event.isRepeating())
                    repeats.add(event);
                else if(event.isSchedulable())
                    events.add(event);
            }
        }

        int sizeOfNonRepeats = events.size();
        System.out.println(events.size());

        for(; events.size() < size + sizeOfNonRepeats;) {
            if(repeats.isEmpty())
                break;

            ScheduledEvent event = repeats.get(0);

            for (int j = 1; j < repeats.size(); j++) {
                ScheduledEvent other = repeats.get(j);

                if (other.isSchedulable() && other.getActivateTime() < event.getActivateTime())
                    event = other;
            }

            if(event.isSchedulable())
                events.add(event.getNextEvent());

            if(!event.isSchedulable())
                repeats.remove(event);
        }

        Collections.sort(events, ScheduledEvent.getTimeNonRepeatComparator());

        // trims events to size
        for(int i = events.size() - 1; i >= size; i--)
            events.remove(i);

        for(int i = 0; i < events.size(); i++)
            if(!events.get(i).isRepeating())
                events.get(i).scheduled();
    }
    public static boolean isEmpty()
    {
        if(events.isEmpty())
            return true;

        return false;
    }

    public static ScheduledEvent deQueue()
    {
        return events.remove(0);
    }
}

